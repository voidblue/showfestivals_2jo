package com.example.voidbluelabtop.showfestivals.JavaUtils;

import android.os.AsyncTask;
import android.util.Log;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.ContentValues.TAG;

public class XmlParseSeogwi extends AsyncTask{
    private ArrayList festivallist;
    private InputStream is;
    private boolean flagEnd;

    public static XmlParseSeogwi getInstacneAndStart(){
        XmlParseSeogwi xmlParseSeogwi = new XmlParseSeogwi();
        xmlParseSeogwi.execute();
        return xmlParseSeogwi;

    }
    private XmlParseSeogwi(){
        flagEnd = false;
        festivallist = new ArrayList();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        xmlParseProcess();
        flagEnd = true;
        return null;
    }

    private void xmlParseProcess(){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();


            URL url = new URL("http://data.seogwipo.go.kr/openapi/service/rest/cultureEvent?ServiceKey=L%2BJigqK1VwOeXGOLROMuYRDsYFo0Ou8AWydVLIgLByjN0S7A3LBxfTqCVp9bYad%2FjU3IZOcjmamd21VcXYmH4Q%3D%3D&numOfRows=500000");
            is = url.openStream();


            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("item");
            System.out.println("-----------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                String[] festival = new String[8];
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    try {
                    festival[0] = getTagValue("ISdate", eElement);
                    festival[1] = getTagValue("MType", eElement);
                    festival[2] = getTagValue("IContents", eElement);
                    festival[3] = getTagValue("ILocation", eElement);
                    festival[4] = getTagValue("IEdate", eElement);
                    festival[5] = getTagValue("PSn", eElement);
                    festival[6] = getTagValue("IHelp", eElement);
                    festival[7] = getTagValue("ISubject", eElement);
                    festivallist.add(festival);
                    }catch (Exception e){
                        continue;
                    }
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "일단 길이 : " + festivallist.size());
    }

    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0);
        return nValue.getNodeValue();
    }

    public ArrayList getFestivalList() {
        return festivallist;
    }

    public boolean getFlag(){
        return flagEnd;
    }
}

