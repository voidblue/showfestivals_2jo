package com.example.voidbluelabtop.showfestivals.Service;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import static android.content.ContentValues.TAG;

/**
 * Created by voidbluelabtop on 17. 12. 6.
 */

public class AsyncReverseGeoCoding extends AsyncTask {
    private HttpURLConnection con;
    private String lat, lng;
    private boolean noResult;
    public AsyncReverseGeoCoding() {
        lat = "0";
        lng = "0";
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        lat = "0";
        lng = "0";
        noResult = false;
        URL url;
        BufferedReader br = null;
        try {
            url = new URL((String) objects[0]+"&key=AIzaSyBvhNTAqrwIlzMghhyUpn0kVpuuyL5Goto");
            con = (HttpURLConnection) url.openConnection();

            br = new BufferedReader(new InputStreamReader(con.getInputStream()));


            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
            }
            br.close();
            System.out.println(response.toString());

            JSONArray ja = new JSONArray("[" + response.toString()+ "]");
            JSONObject jo = ja.getJSONObject(0);

            lat = jo.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lat").toString();

            lng = jo.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lng").toString();

            String result = jo.getJSONArray("status").toString();
            if (result.equals("ZERO_RESULTS")){
                noResult = true;
            }
//                    .getJSONObject(0).getJSONArray("location").getJSONObject(0).getJSONArray("lng").toString();
            Log.d(TAG, "doInBackground: " + "lat = " + lat + "lng");
        } catch (Exception e){
//            e.printStackTrace();
        }
        return null;
    }
    public boolean isOK(){
        if (noResult){
            return true;
        }
        return !lat.equals("0");
    }
    public double getLatitude(){
        return Double.parseDouble(lat);
    }
    public double getLongtitude(){
        return Double.parseDouble(lng);
    }
}
