package com.example.voidbluelabtop.showfestivals.JavaUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by voidbluelabtop on 17. 12. 15.
 */

public class ArrangeFestivalList {
    private ArrayList arrangedList;
    public static ArrangeFestivalList getInstance(){
        return new ArrangeFestivalList();
    }

    private ArrangeFestivalList(){

    }

    public ArrayList arrnge(ArrayList festivalList){
        arrangedList = new ArrayList();
        int[] sequesce = new int[festivalList.size()];
        Date[] dates = new Date[festivalList.size()];
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0 ; i < festivalList.size() ; i++ ){
            sequesce[i] = i;
            try {
                dates[i] = simpleDateFormat.parse(((String[])festivalList.get(i))[4]);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }




        for (int i = 0 ; i < festivalList.size() ; i++){
            for (int j = 0 ; j < festivalList.size() ; j++){
                if (dates[i].compareTo(dates[j]) < 0){
                    int temp = sequesce[i];
                    sequesce[i] = sequesce[j];
                    sequesce[j] = temp;
                    Date tempDate = (Date) dates[i].clone();
                    dates[i] = (Date) dates[j].clone();
                    dates[j] = (Date) tempDate.clone();
                }
            }
        }
        for (int i= 0 ; i < festivalList.size() ; i++){
            arrangedList.add(festivalList.get(sequesce[i]));
        }

        return (ArrayList)arrangedList.clone();
    }
}
