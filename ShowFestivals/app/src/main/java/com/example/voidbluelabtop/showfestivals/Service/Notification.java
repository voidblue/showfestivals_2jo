package com.example.voidbluelabtop.showfestivals.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.voidbluelabtop.showfestivals.Activity.IntroActivity;
import com.example.voidbluelabtop.showfestivals.Activity.Popup_Activity;
import com.example.voidbluelabtop.showfestivals.JavaUtils.CompareDate;
import com.example.voidbluelabtop.showfestivals.R;
import com.example.voidbluelabtop.showfestivals.Singleton.FestivalList;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.ContentValues.TAG;
import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by SSLAB on 2017-11-23.
 */

public class Notification extends Service{
    private static final int NOTIFICATION_ID = 1;

    private NotificationCompat.Builder builder;
    private Intent intent;
    private PendingIntent pendingIntent;
    private NotificationManager notificationManager;
    private CompareDate compareDate;
    private FestivalList festivalList;

    private ArrayList festival;

    private SimpleDateFormat simpleDateFormat;
    private TimerTask t2;
    private Date date;
    private Timer timer2;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private boolean goingtostart;
    private boolean goingtoend;
    private boolean flag_compare;

    public Notification() {
        super();
    }

    public static void setDays(int startMonth, int startDay,int endMonth, int endDay){
        Date today = new Date();
        //TODO 여기 날짜비교 들어가고 오늘안에 체류기간 있으면 노티피 띄우는게됨
    }
    @Override
    public void onCreate() {
        builder = new NotificationCompat.Builder(this);

        sharedPreferences = getSharedPreferences("stayDate", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        //알림에 나타나는 아이콘
        builder.setSmallIcon(R.drawable.hangshowicon);

        //알림이 클릭되면 이 인텐트가 보내진다
        intent = new Intent(getApplicationContext(), IntroActivity.class);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        //사용자가 알림을 터치하면 인텐트가 보내진다
        builder.setContentIntent(pendingIntent);

        //알림에 표시되는 큰 아이콘
        builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.hangshowicon));

        notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);

        //알림바에 알림을 표시한다
        compareDate = new CompareDate();
        compareDate.putDate("2017-12-3");

        festivalList = FestivalList.INSTANCE;

        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd"); //시간 출력포멧 정함
        Log.d("", "onCreate: 서비스 실행");
        t2 = new TimerTask() {
            @Override
            public void run() {
                sendNotification();
            }
        };
        timer2 = new Timer();
        TimerTask t1 = new TimerTask() {
            //TimerTask 추상클래스를 선언하자마자 run()을 강제로 정의하도록 한다.
            @Override
            public void run() {
                date = new Date();
                compareFestivalDate();
            }

        };
        Timer timer =  new Timer();
        timer.schedule(t1, 0, 60000);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {


        return null;
    }

    public void sendNotification() {
            //알림바에 알림을 표시한다
            builder.setWhen(System.currentTimeMillis());
            notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    public void compareFestivalDate() {
        String[] festivalDay;
        String arrive, departure;
        Date today = new Date();

       festival = festivalList.getFestivalList();


       arrive = sharedPreferences.getString("1", "0000-00-00");
       departure = sharedPreferences.getString("2", "0000-00-00");

       if(arrive.equals("0000-00-00") || departure.equals("0000-00-00")){
           return;
       }

       Date arriveDate = simpleDateFormat.parse(arrive, new ParsePosition(0));
       Date departureDate = simpleDateFormat.parse(departure, new ParsePosition(0));

       if(arriveDate == null || departureDate == null){
           Log.d("arrive", arrive);
           Log.d("departure", departure);
           Log.d("date", arriveDate.toString());
           Log.d("date", departureDate.toString());
           throw new RuntimeException("파싱에러");
       }
        Log.d(TAG, "낢ㄴㅇㅁㄴㄹ " + today.toString() );
        Log.d(TAG, "낢ㄴㅇㅁㄴㄹ " + arriveDate.toString() );
        Log.d(TAG, "낢ㄴㅇㅁㄴㄹ " + departureDate.toString() );
       if (arriveDate.compareTo(today) < 0 && departureDate.compareTo(today) > 0) {
           Log.d(TAG, "일단 이프문 통과");
           for (int i = 0; i < festival.size(); i++) {
               festivalDay = (String[]) festival.get(i);
               compareDate.putDate(festivalDay[0]);
               //시작하기 하루전
               if (compareDate.compareDate(1) == 0) {
                   //알림 제목
                   builder.setContentTitle(festivalDay[7]);
                   //알림 콘텐츠
                   builder.setContentText("오늘은 즐거운 축제가 시작하기 하루 전입니다.^^");
                   // 4.2 이상인 경우에 보여지는 서브 텍스트
                   builder.setSubText("Go to google");

                   Timer timer2 = new Timer();

                   if (date.getHours() == 9 && date.getMinutes() == 30) {
                       timer2.schedule(t2, 0, 86400000);
                   }
               }
               compareDate.putDate(festivalDay[4]);
               //끝나기 하루전
               if (compareDate.compareDate(1) == 0) {
                   //알림 제목
                   builder.setContentTitle(festivalDay[7]);

                   //알림 콘텐츠
                   builder.setContentText("오늘은 즐거운 축제가 끝나기 하루 전입니다.^^");

                   // 4.2 이상인 경우에 보여지는 서브 텍스트
                   builder.setSubText("앱으로 이동");

                   Timer timer2 = new Timer();
                   if (date.getHours() == 3 && date.getMinutes() == 56) {
                       timer2.schedule(t2, 0, 86400000);
                   }
               }
           }
       }
    }
}
