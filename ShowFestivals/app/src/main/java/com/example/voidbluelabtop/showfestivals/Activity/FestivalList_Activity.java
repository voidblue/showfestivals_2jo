package com.example.voidbluelabtop.showfestivals.Activity;

/**
 * Created by user on 2017-11-15.
 */
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.voidbluelabtop.showfestivals.Adapter.ListViewAdapter;
import com.example.voidbluelabtop.showfestivals.R;
import com.example.voidbluelabtop.showfestivals.Singleton.FestivalList;

import java.util.ArrayList;

public class FestivalList_Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FestivalList objFestivalList = FestivalList.INSTANCE;
        setTitle("축제 리스트");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0096FA")));
        getWindow().setStatusBarColor(Color.parseColor("#0096FA"));
        setContentView(R.layout.activity_festvallist);

        ListView listview ;
        ListViewAdapter adapter;
        ArrayList festivalList = new ArrayList();
        Bundle data = getIntent().getExtras();
        String mode = (String)data.get("mode");
        Log.d("모드", "onCreate: " + mode);
        // Adapter 생성
        if(mode.equals("jeju")){
            festivalList = objFestivalList.getJejuFestivalList();
        }
        else if(mode.equals("seogwi")){
            festivalList = objFestivalList.getSeogwiFestivalList();
            Log.d("", "onCreate: " + "써귀포 가져온거 맞지 ??");
        }
        adapter = new ListViewAdapter(this, festivalList) ;

        // 리스트뷰 참조 및 Adapter달기
        listview = (ListView) findViewById(R.id.listview1);
        listview.setAdapter(adapter);

        // 위에서 생성한 listview에 클릭 이벤트 핸들러 정의.
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // get item
                String[] item = (String[]) parent.getItemAtPosition(position) ;
                Intent i = new Intent(getApplicationContext(), Festivalinformation_Activity.class);
                i.putExtra("name",item[7]);
                i.putExtra("date",item[0].substring(0,10) + " ~ " + item[4].substring(0,10));
                i.putExtra("place", item[3]);
                i.putExtra("info", item[2]);
                i.putExtra("psn", item[5]);
                startActivity(i);
            }
        }) ;


}


}