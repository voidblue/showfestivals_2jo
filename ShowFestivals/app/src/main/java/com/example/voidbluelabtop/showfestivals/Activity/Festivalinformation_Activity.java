package com.example.voidbluelabtop.showfestivals.Activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.voidbluelabtop.showfestivals.JavaUtils.GetDrawable;
import com.example.voidbluelabtop.showfestivals.Service.AsyncReverseGeoCoding;
import com.example.voidbluelabtop.showfestivals.R;
import com.example.voidbluelabtop.showfestivals.Singleton.GetMyLocation;
import com.kakao.kakaonavi.KakaoNaviParams;
import com.kakao.kakaonavi.KakaoNaviService;
import com.kakao.kakaonavi.Location;
import com.kakao.kakaonavi.NaviOptions;
import com.kakao.kakaonavi.options.CoordType;
import com.kakao.kakaonavi.options.RpOption;
import com.kakao.kakaonavi.options.VehicleType;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

/**
 * Created by user on 2017-11-22.
 */

public class Festivalinformation_Activity extends AppCompatActivity {
    MapView mapView;
    GetMyLocation myLocation;
    Activity thisActivity;
    Bundle data;
    AsyncReverseGeoCoding async;
    @Override
    protected void onCreate(Bundle saveInitInstance){
        super.onCreate(saveInitInstance);
        getWindow().setStatusBarColor(Color.parseColor("#0096FA"));
        setContentView(R.layout.activity_festivalinformation);
        thisActivity = this;
        myLocation = GetMyLocation.INSTANCE;

        mapView = new MapView(this);

        ViewGroup mapViewContainer = (ViewGroup) findViewById(R.id.map);
        mapViewContainer.addView(mapView);
        mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(33.376583333, 126.526222222), true);//제주도 중짐 좌표
        mapView.setZoomLevel(9,true);


        MapPOIItem myplace = new MapPOIItem();
        myplace.setItemName("현재위치");
        myplace.setTag(0);
        myplace.setMapPoint(MapPoint.mapPointWithGeoCoord(myLocation.getLatitude(), myLocation.getLongtitude()));
        myplace.setMarkerType(MapPOIItem.MarkerType.BluePin); // 기본으로 제공하는 BluePin 마커 모양.
        mapView.addPOIItem(myplace);



        data = getIntent().getExtras();

        TextView name = (TextView)findViewById(R.id.festivalname);
        TextView date = (TextView)findViewById(R.id.festivaldate);
        TextView place = (TextView)findViewById(R.id.festivalplace);
        TextView info = (TextView)findViewById(R.id.festivalinfo);
        ImageView imageView = (ImageView)findViewById(R.id.festivalimage);

        name.setText((String)data.get("name"));
        date.setText((String)data.get("date"));
        place.setText((String)data.get("place"));
        GetDrawable getDrawable = GetDrawable.getInstance();
        Drawable image = ContextCompat.getDrawable(this, getDrawable.getDrawable((String)data.get("psn")));
        imageView.setImageDrawable(image);

        //html코드 따온거라 html전용 코드 제거
        String replaceInfo = ((String)data.get("info")).replace("&nbsp;","");
        replaceInfo = replaceInfo.replace("&nbsp;","");
        replaceInfo = replaceInfo.replace("&amp;","");
        replaceInfo = replaceInfo.replace("&lt;","");
        replaceInfo = replaceInfo.replace("&gt;","");
        replaceInfo = replaceInfo.replace("&quot;","");
        replaceInfo = replaceInfo.replace("&ldquo;","");
        replaceInfo = replaceInfo.replace("&#39;","");
        replaceInfo = replaceInfo.replace("&","");
        info.setText(replaceInfo);

        Button btn_car = (Button) findViewById(R.id.btn1);
        async = new AsyncReverseGeoCoding();
        String festivalPlace = (String)data.get("place");
        if (festivalPlace.equals("소극장") || festivalPlace.equals("대극장")){
            festivalPlace = "서귀포 예술의전당";
        }
        async.execute("https://maps.googleapis.com/maps/api/geocode/json?address="+festivalPlace);
        while(true){
            if (async.isOK()){
                break;
            }
        }
        Log.d("좌표", "onCreate: " +async.getLatitude() +"asdsad "+async.getLongtitude());

        MapPOIItem targetplace = new MapPOIItem();
        targetplace.setItemName((String)data.get("name"));
        targetplace.setTag(0);
        targetplace.setMapPoint(MapPoint.mapPointWithGeoCoord(async.getLatitude(),async.getLongtitude()));
        targetplace.setMarkerType(MapPOIItem.MarkerType.RedPin);
        Log.d("", "onCreate: " + "목적지 핀");
        mapView.addPOIItem(targetplace);

        btn_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Location destination = Location.newBuilder((String)data.get("name"), async.getLongtitude(),async.getLatitude()).build();
                NaviOptions options = NaviOptions.newBuilder().setCoordType(CoordType.WGS84).setVehicleType(VehicleType.FIRST).setRpOption(RpOption.SHORTEST).build();


                KakaoNaviParams.Builder builder = KakaoNaviParams.newBuilder(destination).setNaviOptions(options);
                KakaoNaviParams params = builder.build();

                KakaoNaviService.navigate(thisActivity, params);
            }
        });

    }

}
