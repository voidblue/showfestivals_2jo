package com.example.voidbluelabtop.showfestivals.Activity;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.voidbluelabtop.showfestivals.R;

/**
 * Created by voidbluelabtop on 17. 12. 8.
 */

public class ChooseDayDialog extends Dialog{
    public ChooseDayDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.dialog_chooseday);
    }
}
