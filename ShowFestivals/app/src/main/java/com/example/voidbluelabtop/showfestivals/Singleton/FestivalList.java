package com.example.voidbluelabtop.showfestivals.Singleton;

import java.util.ArrayList;

/**
 * Created by voidbluelabtop on 17. 11. 23.
 */

public enum FestivalList {
    INSTANCE;

    private ArrayList festivalList;
    private ArrayList jejuFestivalList;
    private ArrayList seogwiFestivalList;

    FestivalList(){
        festivalList = new ArrayList();
    }
    public void setFestivalList(ArrayList festivalList){
        this.festivalList = festivalList;
    }
    public void setJejuFestivalList(ArrayList jejuFestivalList){
        this.jejuFestivalList = jejuFestivalList;
    }
    public  void setSeogwiFestivalList(ArrayList seogwiFestivalList){
        this.seogwiFestivalList = seogwiFestivalList;
    }
    public ArrayList getFestivalList(){
        return festivalList;
    }
    public ArrayList getSeogwiFestivalList(){return seogwiFestivalList;}
    public ArrayList getJejuFestivalList(){return jejuFestivalList;}
}
