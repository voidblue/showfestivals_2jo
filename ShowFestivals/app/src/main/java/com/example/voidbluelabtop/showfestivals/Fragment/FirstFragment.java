package com.example.voidbluelabtop.showfestivals.Fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.voidbluelabtop.showfestivals.Activity.Festivalinformation_Activity;
import com.example.voidbluelabtop.showfestivals.JavaUtils.GetDrawable;
import com.example.voidbluelabtop.showfestivals.R;
import com.example.voidbluelabtop.showfestivals.Singleton.FestivalList;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by user on 2017-12-08.
 */

public class FirstFragment extends Fragment
{
    FestivalList festivalList;
    String[] festival;
    boolean doNotLoad = false;
    public FirstFragment()
    {
        festivalList = FestivalList.INSTANCE;
        try {
            festival = (String[]) ((ArrayList) festivalList.getJejuFestivalList()).get(0);
        }catch (Exception e){
            System.out.println(e.getMessage());
            doNotLoad = true;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

            RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.fragment_first, container, false);
        if(!doNotLoad) {
            RelativeLayout  bgimage = layout.findViewById(R.id.fgbg1);
            TextView name = layout.findViewById(R.id.fgName1);
            TextView time = layout.findViewById(R.id.fgTime);
            Log.d(TAG, "onCreateView: " + festival[4]);
            name.setText(festival[7]);
            time.setText(festival[0].substring(0, 10) + " ~ " + festival[4].substring(0, 10));

            GetDrawable getDrawable = GetDrawable.getInstance();
            if (getDrawable.getDrawable(festival[5]) != 0) {
                Drawable Image = ContextCompat.getDrawable(getActivity(), getDrawable.getDrawable(festival[5]));
                bgimage.setBackground(Image);
            }

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity().getApplicationContext(), Festivalinformation_Activity.class);
                    i.putExtra("name", festival[7]);
                    i.putExtra("date", festival[0].substring(0, 10) + " ~ " + festival[4].substring(0, 10));
                    i.putExtra("place", festival[3]);
                    i.putExtra("info", festival[2]);
                    i.putExtra("psn", festival[5]);
                    startActivity(i);
                }
            });
        }


        return layout;

    }
}

