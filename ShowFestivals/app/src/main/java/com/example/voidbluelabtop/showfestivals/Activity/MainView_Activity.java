package com.example.voidbluelabtop.showfestivals.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.voidbluelabtop.showfestivals.Fragment.FirstFragment;
import com.example.voidbluelabtop.showfestivals.Fragment.SecondFragment;
import com.example.voidbluelabtop.showfestivals.Fragment.ThirdFragment;
import com.example.voidbluelabtop.showfestivals.JavaUtils.CompareDate;
import com.example.voidbluelabtop.showfestivals.JavaUtils.GetKeyHash;
import com.example.voidbluelabtop.showfestivals.JavaUtils.XmlParseJeju;
import com.example.voidbluelabtop.showfestivals.JavaUtils.XmlParseSeogwi;
import com.example.voidbluelabtop.showfestivals.R;
import com.example.voidbluelabtop.showfestivals.Service.Notification;
import com.example.voidbluelabtop.showfestivals.Singleton.FestivalList;
import com.example.voidbluelabtop.showfestivals.Singleton.GetMyLocation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by user on 2017-12-08.
 */

public class MainView_Activity extends AppCompatActivity
{
    ViewPager vp;
    GetMyLocation myLocation;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(Color.parseColor("#0096FA"));
        setContentView(R.layout.activity_mainview);
        context = this;
        vp = (ViewPager)findViewById(R.id.vp);

        //viewpager adapter연결결
        vp.setAdapter(new pagerAdapter(getSupportFragmentManager()));
        //앱 실행됐을 때 첫번째 페이지로 초기화
        vp.setCurrentItem(0);



        Button btnJeju = (Button) findViewById(R.id.btn_jeju);
        Button btnSeogwi = (Button) findViewById(R.id.btn_seogwi);

        btnJeju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), FestivalList_Activity.class);
                i.putExtra("mode", "jeju");
                startActivity(i);
            }
        });

        btnSeogwi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), FestivalList_Activity.class);
                i.putExtra("mode", "seogwi");
                startActivity(i);
            }
        });

        ImageView setting = (ImageView)findViewById(R.id.image_setting);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Popup_Activity pa = new Popup_Activity(context);
                Toast.makeText(getApplicationContext(), "제주에 도착하는 날짜를 선택하고 확인을 눌러주세요",Toast.LENGTH_LONG).show();
                pa.show();
            }
        });

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        boolean flag = permissionCheck == PackageManager.PERMISSION_DENIED;
        if(permissionCheck == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        ArrayList festivalList = new ArrayList();
        myLocation= GetMyLocation.INSTANCE;
        myLocation.setContext(getApplicationContext(), this);



        GetKeyHash keyHash = GetKeyHash.getInstance(this);
        keyHash.getKeyHash(this);
    }

    View.OnClickListener movePageListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            int tag = (int) v.getTag();
            vp.setCurrentItem(tag);
        }
    };




    private class pagerAdapter extends FragmentStatePagerAdapter
    {
        public pagerAdapter(android.support.v4.app.FragmentManager fm)
        {
            super(fm);
        }
        @Override
        public android.support.v4.app.Fragment getItem(int position)
        {
            switch(position)
            {
                case 0:
                    return new FirstFragment();
                case 1:
                    return new SecondFragment();
                case 2:
                    return new ThirdFragment();
                default:
                    return null;
            }
        }
        @Override
        //getCount() ViewPager 안에 들어가는 페이지 개수
        public int getCount()
        {
            return 3;
        }
    }




}

