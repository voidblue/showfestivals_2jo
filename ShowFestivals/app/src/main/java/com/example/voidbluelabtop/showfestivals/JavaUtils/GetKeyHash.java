package com.example.voidbluelabtop.showfestivals.JavaUtils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.kakao.util.maps.helper.Utility.getPackageInfo;

/**
 * Created by voidbluelabtop on 17. 11. 17.
 */

public class GetKeyHash {
    Activity parent;

    private GetKeyHash(Activity activity) {
        parent = activity;
    }

    public static  GetKeyHash getInstance(Activity activity) {
        return new GetKeyHash(activity);
    }

    public String getKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null) {
            Log.d("해시 키", "해시 키 없음");
        }
            //return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("해시 키", ""+Base64.encodeToString(md.digest(), Base64.NO_WRAP));
                return Base64.encodeToString(md.digest(), Base64.NO_WRAP);
            } catch (NoSuchAlgorithmException e) {
                Log.w("해시 키 ", "Unable to get MessageDigest. signature=" + signature, e);
            }
        }
        return null;
    }
}
