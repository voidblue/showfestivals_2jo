package com.example.voidbluelabtop.showfestivals.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.example.voidbluelabtop.showfestivals.JavaUtils.ArrangeFestivalList;
import com.example.voidbluelabtop.showfestivals.JavaUtils.CompareDate;
import com.example.voidbluelabtop.showfestivals.JavaUtils.XmlParseJeju;
import com.example.voidbluelabtop.showfestivals.JavaUtils.XmlParseSeogwi;
import com.example.voidbluelabtop.showfestivals.R;
import com.example.voidbluelabtop.showfestivals.Service.Notification;
import com.example.voidbluelabtop.showfestivals.Singleton.FestivalList;

import java.util.ArrayList;
import java.util.logging.LogRecord;

/**
 * Created by voidbluelabtop on 17. 12. 14.
 */

public class IntroActivity extends Activity {
    private Handler handler;

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            setFestvalList();
            Intent intent = new Intent(IntroActivity.this, MainView_Activity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        Intent i = new Intent(getApplicationContext(), Notification.class);
        startService(i);
        init();
        handler.postDelayed(runnable, 500);
    }

    public void init() {
        handler = new Handler();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        handler.removeCallbacks(runnable);
    }




    private void setFestvalList(){
        ;
        ArrayList festivalList = new ArrayList();
        ArrayList jejuFestivalList = new ArrayList();
        ArrayList seogwiFestivalList = new ArrayList();
        CompareDate compareDate = new CompareDate();
        XmlParseJeju jejuxp = XmlParseJeju.getInstanceAndStart();
        XmlParseSeogwi seogwixp = XmlParseSeogwi.getInstacneAndStart();
        while(true){
            if(jejuxp.getFlag()) break;
        }
        ArrayList tempFestList = jejuxp.getFestivalList();

        for(int i = 0 ; i < tempFestList.size() ; i++ ) {
            String[] item = (String[])tempFestList.get(i);
            Log.d("시간", "시작  : " + item[0] + "끝 : " + item[4]);
            compareDate.putDate(item[0]);
            boolean startOK, endOK;
            startOK = compareDate.compareDate(30) <= 0;
            compareDate.putDate(item[4]);
            endOK = compareDate.compareDate(0) >= 0;
            if (startOK && endOK) {
                festivalList.add(item);
                jejuFestivalList.add(item);
            }
        }
        Log.d("제주 페스티벌 리스트 길이", "" + tempFestList.size());
        while(true){
            if(seogwixp.getFlag()) break;
        }
        tempFestList = seogwixp.getFestivalList();

        for(int i = 0 ; i < tempFestList.size() ; i++ ) {
            String[] item = (String[])tempFestList.get(i);
            Log.d("", "setFestvalList: " + item[0] + " dasdsad    " + item[4]);
            compareDate.putDate(item[0]);
            boolean startOK, endOK;
            startOK = compareDate.compareDate(30) <= 0;
            compareDate.putDate(item[4]);
            endOK = compareDate.compareDate(0) >= 0;
            if (startOK && endOK) {
                festivalList.add(item);
                seogwiFestivalList.add(item);
            }
        }
        Log.d("서귀 페스티벌 리스트 길이", "" + tempFestList.size());


        ArrangeFestivalList arrangeFestivalList = ArrangeFestivalList.getInstance();
        festivalList = arrangeFestivalList.arrnge(festivalList);
        seogwiFestivalList = arrangeFestivalList.arrnge(seogwiFestivalList);
        jejuFestivalList = arrangeFestivalList.arrnge(jejuFestivalList);

        FestivalList objFestivalList = FestivalList.INSTANCE;
        objFestivalList.setFestivalList(festivalList);
        objFestivalList.setJejuFestivalList(jejuFestivalList);
        objFestivalList.setSeogwiFestivalList(seogwiFestivalList);
    }
}