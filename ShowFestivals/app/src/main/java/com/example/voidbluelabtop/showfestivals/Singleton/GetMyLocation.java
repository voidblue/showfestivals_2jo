package com.example.voidbluelabtop.showfestivals.Singleton;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by voidbluelabtop on 17. 11. 15.
 */

public enum GetMyLocation {
    INSTANCE;

    private double longitude; //경도
    private double latitude;   //위도
    private double altitude;   //고도
    private float accuracy;    //정확도
    private LocationManager lm;
    private Activity parent;
    private Context applicationContext;

    public void setContext(Context context, Activity activity){
        applicationContext = context;
        parent = activity;
        settingStart();
    }


    // LocationManager 객체를 얻어온다

    private void settingStart() {
        lm = (LocationManager) applicationContext.getSystemService(Context.LOCATION_SERVICE);

        int permissionCheck = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION);

        if(permissionCheck== PackageManager.PERMISSION_DENIED){

            // 권한 없음
        }else{

            ActivityCompat.requestPermissions(parent,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, // 등록할 위치제공자
                100, // 통지사이의 최소 시간간격 (miliSecond)
                1, // 통지사이의 최소 변경거리 (m)
                mLocationListener);
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, // 등록할 위치제공자
                100, // 통지사이의 최소 시간간격 (miliSecond)
                1, // 통지사이의 최소 변경거리 (m)
                mLocationListener);
        // LocationManager 객체를 얻어온다


    }


    public double getLongtitude(){
        return longitude;
    }

    public double getLatitude(){
        return latitude;
    }
    public double getAltituyde(){
        return altitude;
    }
    private final LocationListener mLocationListener = new LocationListener() {

        public void onLocationChanged(Location location) {
            //여기서 위치값이 갱신되면 이벤트가 발생한다.
            //값은 Location 형태로 리턴되며 좌표 출력 방법은 다음과 같다.

            Log.d("test", "onLocationChanged, location:" + location);
            longitude = location.getLongitude(); //경도
            latitude = location.getLatitude();   //위도
            altitude = location.getAltitude();   //고도
            accuracy = location.getAccuracy();    //정확도
            String provider = location.getProvider();   //위치제공자
            //Gps 위치제공자에 의한 위치변화. 오차범위가 좁다.
            //Network 위치제공자에 의한 위치변화
            //Network 위치는 Gps에 비해 정확도가 많이 떨어진다.
        }
        public void onProviderDisabled(String provider) {
            // Disabled시
            Log.d("test", "onProviderDisabled, provider:" + provider);
        }

        public void onProviderEnabled(String provider) {
            // Enabled시
            Log.d("test", "onProviderEnabled, provider:" + provider);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            // 변경시
            Log.d("test", "onStatusChanged, provider:" + provider + ", status:" + status + " ,Bundle:" + extras);
        }

    };
    public void show() {
        Toast.makeText(applicationContext, "위도 = " + latitude + "\n경도 = " + longitude, Toast.LENGTH_SHORT).show();
//        lm.removeUpdates(mLocationListener);  //  미수신할때는 반드시 자원해체를 해주어야 한다.
    }
} // end of class

