package com.example.voidbluelabtop.showfestivals.JavaUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


//쓸필요 없는듯 일단 보류
class DateTime {
    private long now;
    private Date date, festivalToday;
    private SimpleDateFormat simpleDateFormat;
    private String today;

    public DateTime() {
        this.now = System.currentTimeMillis();                               //현재시간을 msec으로 구한다
        this.date = new Date(now);                                           //현재시간을 date변수에 저장
        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd"); //시간 출력포멧 정함
        this.today = simpleDateFormat.format(date);                          //오늘 날짜를 저장할 변수

        try {
            festivalToday = simpleDateFormat.parse(this.today);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void showToday() {
        System.out.println(today);
    }

    public Date getDate(String Date) {
        return festivalToday;
    }
}
