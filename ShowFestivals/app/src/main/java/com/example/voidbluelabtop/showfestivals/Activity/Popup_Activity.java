package com.example.voidbluelabtop.showfestivals.Activity;

import android.app.Dialog;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.voidbluelabtop.showfestivals.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by user on 2017-12-10.
 */

public class Popup_Activity extends Dialog {
    private Context parent;
    private String startDate;
    private String endDate;
    boolean isFirst;

    SharedPreferences stayDate;
    SharedPreferences.Editor editor;

    private int keyValue;

    public Popup_Activity(Context context) {
        super(context);
        parent = context;

        this.keyValue = 0;
        stayDate = parent.getSharedPreferences("stayDate", MODE_PRIVATE);
        editor = stayDate.edit();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //타이틀바 없애기

        setContentView(R.layout.activity_calendar_popup);
        isFirst = true;
        final DatePicker datePicker = findViewById(R.id.datepicker);
        Button btn_accept = findViewById(R.id.btnaccept);
        Button btn_cancel = findViewById(R.id.btncancel);

        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isFirst) {
                    startDate = Integer.toString(datePicker.getYear());
                    startDate += "-" + Integer.toString(datePicker.getMonth()+1);
                    startDate += "-" + Integer.toString(datePicker.getDayOfMonth());
                    Toast.makeText(parent.getApplicationContext(), "제주에서 떠나시는 날을 선택하고 확인을 눌러주세요", Toast.LENGTH_LONG).show();
                    isFirst = false;
                }
                else if(!isFirst){
                    endDate = Integer.toString(datePicker.getYear());
                    endDate += "-" + Integer.toString(datePicker.getMonth()+1);
                    endDate += "-" + Integer.toString(datePicker.getDayOfMonth());

                    Toast.makeText(parent.getApplicationContext(), "체류기간이 : " + startDate + "에서\n" +endDate +"로 설정되었습니다.", Toast.LENGTH_LONG).show();
                    Intent notify = new Intent(parent.getApplicationContext(), com.example.voidbluelabtop.showfestivals.Service.Notification.class);
                    setStayDate();
                    parent.startService(notify);
                    dismiss();

                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setStayDate() {
        editor.putString("1", startDate);
        editor.putString("2", endDate);
        editor.commit();
    }

    public String getArrived() {
        String arrive = stayDate.getString("1", "0000-00-00");
        return arrive;
    }
}