package com.example.voidbluelabtop.showfestivals.JavaUtils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


//축제 날짜를 받아와서 비교하는것
public class CompareDate {
    private String festival;
    private DateTime dateTime;
    private Date today, festivalDay;

    private SimpleDateFormat simpleDateFormat;

    //오늘날짜만 포맷에 맞춰서 가져오기
    public CompareDate() {

        /** 23행 날짜 가져오는 부분
         *Notification class의 sendNotification() 메소드
         * +1 빼니까 notification에서 비교할때 compareTo 리턴값 잘 맞는것 같음
         * +1 붙이니까 잘 안맞음
         * */
        today = new Date();
        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd"); //시간 출력포멧 정함
        String strToday = simpleDateFormat.format(today);
        try {
            today = simpleDateFormat.parse(strToday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //축제 날짜 넣어서 포맷에 맞추기
    public void putDate(String date){
        try {
            this.festivalDay = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //오늘부터 몇일까지 출력할지 정수형으로 넣어주고 유요한지 알아내기
    public int compareDate(int invalidDay) {
        festivalDay.setDate(festivalDay.getDate() - invalidDay);
        Log.d("축제날  ", "" + festivalDay.toString());

        return festivalDay.compareTo(today);
    }
}
