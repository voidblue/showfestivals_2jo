package com.example.voidbluelabtop.showfestivals.JavaUtils;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.example.voidbluelabtop.showfestivals.R;

/**
 * Created by voidbluelabtop on 17. 12. 15.
 */

public class GetDrawable {
    public static GetDrawable getInstance(){
        return new GetDrawable();
    }

    private GetDrawable(){

    }
    public int getDrawable(String PSn){
        int drawable = 0;
        if(PSn.equals("0000000708")){ //헨델의메시아
            drawable = R.drawable.handell;
        }
        else if(PSn.equals("0000000806")){ //효례 국악 한마당
            drawable = R.drawable.gukak;
        }
        else if(PSn.equals("0000000814")){ //아르모니아윈드앙상블
            drawable = R.drawable.armonia;
        }
        else if(PSn.equals("0000000815")){ //2017년 기획전시 제주정신
            drawable = R.drawable.exhibit;
        }
        else if(PSn.equals("0000000818")){ //산방산, 형제섬 그리고 박수기정
            drawable = R.drawable.sanbang;
        }
        else if(PSn.equals("0000000822")){ //김양남 피아노 독주회
            drawable = R.drawable.piano;
        }
        else if(PSn.equals("0000000839")){ //이수일과 심순애
            drawable = R.drawable.leesu;
        }
        else if(PSn.equals("0000000840")){ //도립서귀포합창단 정기평정
            drawable = R.drawable.chorus;
        }
        else if(PSn.equals("0000000843")){ //2017공직자화합한마당페스티벌
            drawable = R.drawable.gongjik;
        }
        else if(PSn.equals("0000000882")){ //12월기획_이수일과 심순애
            drawable = R.drawable.leesu;
        }
        else if(PSn.equals("0000000886")){ //연필과 지우개
            drawable = R.drawable.pencil;
        }
        else if(PSn.equals("0000000889")){ //12월기획영화_동주
            drawable = R.drawable.donju;
        }
        else if(PSn.equals("0000000890")){ //홍신자의 거울
            drawable = R.drawable.mirror;
        }
        else if(PSn.equals("0000000683")){ //2017 송년음악회 무대설치
            drawable = R.drawable.soegwi_perform;
        }
        else if(PSn.equals("0000000684")){ //2017 송년음악회
            drawable = R.drawable.soegwi_perform;
        }
        else if(PSn.equals("0000000893")){ //제주신화속 꽃 상징성 의미연구
            drawable = R.drawable.jeju_flower;
        }
        else if(PSn.equals("0000000894")){ //서귀포를 춤추다\
            drawable = R.drawable.dance_soegwi;
        }
        else if(PSn.equals("0000000895")){ //이중섭미술관_미도파화랑 상상
            drawable = R.drawable.leejungsoeb;
        }
        else if(PSn.equals("0000000897")){ //이수일과 심순애_무대철거
            drawable = R.drawable.leesu;
        }
        else if(PSn.equals("0000000897")){ //이수일과 심순애_무대철거
            drawable = R.drawable.leesu;
        }
        else if(PSn.equals("0000000897")){ //이수일과 심순애_무대철거
            drawable = R.drawable.leesu;
        }
        else if(PSn.equals("0000000881")){ //비바리의 꿈
            drawable = R.drawable.viva;
        }
        else if(PSn.equals("0000000891")){ //소암 현중화 선생 추모전
            drawable = R.drawable.soam;
        }
        else{
            drawable = R.drawable.noimage;
        }
        //여기부터 제주시
        if(PSn.equals("500")){ //베토벤 교향곡 합창
            drawable = R.drawable.beethoven;
        }
        return drawable;
    }
}
