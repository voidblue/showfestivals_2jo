package com.example.voidbluelabtop.showfestivals.JavaUtils;


import android.net.Uri;
import android.os.AsyncTask;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
 
public class XmlParseJeju extends AsyncTask{
	private ArrayList festivallist;
	private InputStream is;
	private boolean flagEnd;

	public static XmlParseJeju getInstanceAndStart()
	{
		XmlParseJeju xmlParseJeju = new XmlParseJeju();
		xmlParseJeju.execute();
		return xmlParseJeju;
	}

	private XmlParseJeju(){
		flagEnd = false;
		festivallist = new ArrayList();
	}
	private void xmlParseProcess(){

		try {
            URL url = new URL("http://210.99.248.79/rest/FestivalInquiryService/getFestivalList?startPage=1&pageSize=20&authApiKey=L%2BJigqK1VwOeXGOLROMuYRDsYFo0Ou8AWydVLIgLByjN0S7A3LBxfTqCVp9bYad%2FjU3IZOcjmamd21VcXYmH4Q%3D%3D");
            is = url.openStream();

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();

			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("list");
			System.out.println("-----------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				String[] festival = new String[8];
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					festival[0] = getTagValue("sdate", eElement);        //시작날짜
					try {
						festival[1] = getTagValue("host1", eElement);
					}catch (Exception e){
						try {
							festival[1] = getTagValue("host", eElement);
						}catch (Exception e2){
							festival[1] = "정보없음";
						}
					}
					festival[2] = getTagValue("info", eElement);
					festival[3] = getTagValue("location", eElement);
					festival[4] = getTagValue("edate", eElement);        //끝나는 날짜
					festival[5] = getTagValue("seq", eElement);
					festival[6] = getTagValue("tel", eElement);
					festival[7] = getTagValue("title", eElement);
					System.out.println("잘 가져오고 있니 " + festival[0]);
					festivallist.add(festival);
				}
			}
		}catch (Exception e) {
				e.printStackTrace();
			}
		}
	
  private static String getTagValue(String sTag, Element eElement) {
	  	NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0);
        return nValue.getNodeValue();
  }
  
  public ArrayList getFestivalList(){
	  return festivallist;
  }


	@Override
	protected Object doInBackground(Object[] objects) {
		xmlParseProcess();
        flagEnd = true;
		return null;
	}

	public boolean getFlag(){
		return flagEnd;
	}
}

