package com.example.voidbluelabtop.showfestivals;

import junit.framework.Assert;

import org.junit.Test;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
    @Test
    public void timeParsingTest() throws Exception{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String arrive = "2017-12-15";
        Date arriveDate = simpleDateFormat.parse(arrive, new ParsePosition(0));
        Assert.assertNotNull(arriveDate);
        Assert.assertEquals(117, arriveDate.getYear());
        Assert.assertEquals(11, arriveDate.getMonth());
        Assert.assertEquals(15, arriveDate.getDate());

    }
}